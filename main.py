import os
import yt_dlp
from dotenv import load_dotenv
from telegram import Update
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackContext
from youtube_metadata import get_video_title, get_video_thumbnail, get_video_description
from bot_commands import start, title, thumbnail, description
from youtube_link_handler import handle_youtube_link
from handlers import setup_handlers


# Load environment variables from the .env file and store the TELEGRAM_TOKEN.
load_dotenv()
TELEGRAM_TOKEN = os.getenv('TELEGRAM_TOKEN')


def main():
    #managing and handling updates from the Telegram server, such as new messages or commands sent to the bot.
    updater = Updater(TELEGRAM_TOKEN)

    #The Dispatcher class is responsible for managing and dispatching the updates received by the Updater
    dp = updater.dispatcher
    setup_handlers(dp)

    updater.start_polling()
    updater.idle()

#Execute the main function when the script is run.
if __name__ == '__main__':
    main()
