from telegram import Update
from telegram.ext import CallbackContext

def start(update: Update, context: CallbackContext):
    update.message.reply_text("Hi! I'm a Youtube bot!")

def title(update: Update, context: CallbackContext):
    chat_id = update.message.chat_id
    context.user_data[chat_id] = 'title'
    update.message.reply_text("Send me a YouTube link to get the video title.")

def thumbnail(update: Update, context: CallbackContext):
    chat_id = update.message.chat_id
    context.user_data[chat_id] = 'thumbnail'
    update.message.reply_text("Send me a YouTube link to get the thumbnail URL.")

def description(update: Update, context: CallbackContext):
    chat_id = update.message.chat_id
    context.user_data[chat_id] = 'description'
    update.message.reply_text("Send me a YouTube link, and I'll give you the description of the video.")
