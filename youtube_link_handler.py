from telegram import Update
from telegram.ext import CallbackContext
from youtube_metadata import get_video_title, get_video_thumbnail, get_video_description

# Declare a global variable menu_state to track the current state (title, thumbnail, or description) for each chat.
menu_state = {}

def handle_youtube_link(update: Update, context: CallbackContext):
    chat_id = update.message.chat_id
    url = update.message.text
    current_state = context.user_data.get(chat_id)

    if current_state:
        if current_state == 'title':
            video_title = get_video_title(url)
            update.message.reply_text(f'{video_title}')
        elif current_state == 'thumbnail':
            thumbnail_url = get_video_thumbnail(url)
            update.message.reply_text(f'{thumbnail_url}')
        elif current_state == 'description':
            video_description = get_video_description(url)
            update.message.reply_text(f'{video_description}')
    else:
        update.message.reply_text("Please use menu commands first.")
