# Use the official Python image as the base image
FROM python:3.9-slim

# Set the working directory
WORKDIR /app

# Install necessary packages
RUN apt-get update && \
    apt-get install -y git

# Clone the python-telegram-bot library with the specific version (v13.15)
RUN git clone --branch v13.15 https://github.com/python-telegram-bot/python-telegram-bot
# Change directory and install the library
RUN cd python-telegram-bot && \
    python3 setup.py install

# Install other required Python libraries
COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

# Copy the source code to the working directory
COPY . .

# Set the command to run the application
CMD ["python3", "main.py"]
