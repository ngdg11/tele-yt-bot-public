from telegram.ext import CommandHandler, MessageHandler, Filters
from bot_commands import start, title, thumbnail, description
from youtube_link_handler import handle_youtube_link

def setup_handlers(dp):
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("title", title))
    dp.add_handler(CommandHandler("thumbnail", thumbnail))
    dp.add_handler(CommandHandler('description', description))
    dp.add_handler(MessageHandler(Filters.regex(r'^(https?://)?(www\.)?(m\.)?(youtube\.com|youtu\.be|youtube\.googleapis\.com)/(watch\?v=|embed/|v/|u/\w+/videos/|e/|.+\/)?([^\s]+)'), handle_youtube_link))
